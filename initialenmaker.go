package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	// Controleer of er een naam als command line argument is meegegeven
	if len(os.Args) > 1 {
		name := strings.Join(os.Args[1:], " ")
		initials := extractInitials(name)
		fmt.Println("Initialen:", strings.ToUpper(initials))
	} else {
		// Als er geen command line argument is meegegeven, vraag om input van de gebruiker
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Voer de naam in: ")
		name, _ := reader.ReadString('\n')
		initials := extractInitials(name)
		fmt.Println("Initialen:", strings.ToUpper(initials))
	}
}

func extractInitials(name string) string {
	var initials string
	words := strings.Fields(name)
	for _, word := range words {
		// Sla tussenvoegsels over
		if !isTussenvoegsel(word) {
			// Voeg de eerste letter van elk woord toe aan de initialen
			initials += string(word[0])
		}
	}
	return initials
}

func isTussenvoegsel(word string) bool {
	tussenvoegsels := []string{"de", "den", "der", "van", "van de", "van der", "ten", "ter", "te", "op", "het", "in", "aan", "bij", "onder", "over", "uit", "voor", "achter", "tussen", "voor", "achter", "door", "binnen", "buiten", "langs"}
	for _, tussenvoegsel := range tussenvoegsels {
		if strings.ToLower(word) == tussenvoegsel {
			return true
		}
	}
	return false
}
